﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidateDefault.aspx.cs" Inherits="Soln_ValidationJquery.ValidateDefault" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <title> validate </title>
   <style>
        table{
            width:30%;
            margin:auto;
            border-collapse:collapse;
            border-spacing: 5px;
        }

        th,td {
            border:1px;
            border-style:solid;
            border-color:gray;
            text-align:center;
            padding:10px;
            margin:10px;
        }

        tr:nth-child(even){
            background-color:gray;
        }
    </style>
    <link href="Css/table.css" rel="stylesheet" type ="text/css" /> 
     <script type="text/javascript">

         $(document).ready(function () {
             $('#btnsubmit').click(function () {
                 var txtfirstname = $("#<%=txtfirstname %>").val();
                var txtlastname = $("#<%= txtlastname %>").val();


                if (txtfirstname == "") {
                    $('#divid').css("display", "block");
                    $('#lblerror').html("Please Enter Firstname");
                    $('#txtfirstname').css("border", "1px solid #FF0000");
                    $('#txtfirstname').focus(function () {
                        $('#txtfirstname').css("border", "1px solid #000000");
                        $('#divid').css("display", "none");
                    });
                    return false;
                }
                if (txtlastname == "") {
                    $('#divid').css("display", "block");
                    $('#lblerror').html("Please Enter Lastname");
                    $('#txtlastname').css("border", "1px solid #FF0000");
                    $('#txtlastname').focus(function () {
                        $('#txtlastname').css("border", "1px solid #000000");
                        $('#divid').css("display", "none");
                    });
                    return false;
                }
            });

        });
       </script> 

</head>
<body>
    <form id="form1" runat="server">
        <div class="div_error" id="div1" align="left" style="color: #D8000C; background-color: #FFBABA; border:1px solid #D8000C; width:320px; padding:10px; display:none;" runat="server">
                 
                 </div>
        <div>
            <label id="Label1" runat="server"></label>
            <table>
                       <tr>
                              <td>
                                Firstname :
                                  <asp:TextBox ID="txtfirstname" runat="server" BackColor="orange" BorderColor="#00CC00"></asp:TextBox>
                            </td>
                     </tr>

                        <tr>
                              <td>
                                Lastname :
                                  <asp:TextBox ID="txtlastname" runat="server" BackColor="orange" BorderColor="#006600"></asp:TextBox>
                            </td>                      
                        </tr>

                          <tr>
                              <td>
                          <asp:Button ID="btnsubmit" runat="server" BackColor="#CC66FF" BorderColor="#660066" Text="Submit" />                                                               
                            </td>                      
                         </tr>


               
            </table>

        </div>
    </form>
</body>
</html>
